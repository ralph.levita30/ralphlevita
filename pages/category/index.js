import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function index() {

	const { user } = useContext(UserContext);

	const [categoryName, setCategoryName] = useState("");
	const [categoryType, setCategoryType] = useState('Income');
		console.log(categoryType)
	const [isActive, setIsActive] = useState(false);

	const [categories, setCategories] = useState([]);


		function addCategory(e) {

			e.preventDefault();

			setCategoryName('');
			setCategoryType('');
	 
			const options = {
	           method: 'POST',
	           headers: {Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'},
	            body: JSON.stringify({
	                categoryName: categoryName,
	                categoryType: categoryType
	            })
	        }

	        fetch ('https://arcane-oasis-18921.herokuapp.com/api/categories', options)
	          .then(res => res.json())
	           .then(data => {
	           		if (data === true) {
	           			Swal.fire("Category was added successfully!")
	           			Router.push('/category')
	           		}else {
	           			Swal.fire("oops! something went wrong")
	           		}
	           })
		}

	useEffect(() => {
  

    fetch ('https://arcane-oasis-18921.herokuapp.com/api/categories', {
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }) 
    .then(res => res.json())
    .then(data => {

 
        setCategories(data.map(category => {

        	console.log(category)
  				return (
						<tr key={category._id}>
							<td>{category.categoryName}</td>
						    <td>{category.categoryType}</td>
						</tr>    		

				)

        }))

    })   

	}, [isActive]) 

useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(categoryName !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [categoryName]);

	return(
		<React.Fragment>
            <Head>
                <title>Categories</title>
            </Head>
					
		<Form onSubmit={(e) => addCategory(e)}>
			<Form.Group controlId="category">
				<Form.Label>Category Name</Form.Label>
				<Form.Control
					type="category"
					placeholder="Enter Category Name"
					value={categoryName}
					onChange={(e) => setCategoryName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Category Type</Form.Label>
				<div>
				<select className="w-100 text-center d-flex justify-content-center"
					value={categoryType}
					onChange={(e) => setCategoryType(e.target.value)}
					required
				>
					<option value="Income">Income</option>
					<option value="Expense">Expense</option>
				</select>
				</div>	
			</Form.Group>

			{isActive ?
				<Button variant="info" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
					Submit
				</Button>
				:
				<Button variant="secondary" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
					Submit
				</Button>
			}
		</Form>
		<hr/>
		<h3 className="text-center"> Category List </h3>
		<Table bordered hover className="text-center">
			<thead>
				<tr>
					<th>Category Name</th>
					<th>Category Type</th>
				</tr>
			</thead>
			<tbody>	
				{categories}
			</tbody>
		</Table>	
		</React.Fragment>
	)

}