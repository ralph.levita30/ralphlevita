import React, { useContext, useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import Record from '../../components/Record';
import Head from 'next/head';
import { Button, Table, Form } from 'react-bootstrap';
import Link from 'next/link';
export default function index() {

	const [records, setRecords] = useState([]);

	const [term, setTerm] = useState('');
	const [filter, setFilter] = useState('');
	const [balance, setBalance] = useState ('');
	const [labels, setLabels] = useState ([]);
	const [transactions, setTransactions] = useState ([]);

	useEffect (() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch('https://arcane-oasis-18921.herokuapp.com/api/records/', options)
        .then (res => res.json())
        .then(data=>{
        	console.log(data)
			setRecords(data.records)
			setBalance(data.balance)
			setTransactions(data)
		})

	} ,[])

			console.log(records)
			const recordsData = transactions.map(data => {
    		console.log(data.type)

				if (filter === "Income" && data.categoryType === filter){
					if (term !== '') {
						         																						
						if (data.categoryName.toLowerCase().includes(term.toLowerCase()) || data.categoryType.toLowerCase().includes(term.toLowerCase())){
							return <Record key= {data._id} record= {data} />       									
						} else {
							return null
						}
					} else {
						return <Record key= {data._id} record= {data} />     											
					}

				} else if (filter === "Expense" && data.categoryType === filter){
					if (term !== '') {
						         																						
						if (data.categoryName.toLowerCase().includes(term.toLowerCase()) || data.categoryType.toLowerCase().includes(term.toLowerCase())){
							return <Record key= {data._id} record= {data} />       									
						} else {
							return null
						}
					} else {
						return <Record key= {data._id} record= {data} />     											
					}

				} else if (filter !== "Income" && filter !== "Expense"){
					if (term !== '') {
						         																						
						if (data.categoryName.toLowerCase().includes(term.toLowerCase()) || data.description.toLowerCase().includes(term.toLowerCase())){
							return <Record key= {data._id} record= {data} />       									
						} else {
							return null
						}
					} else {
						return <Record key= {data._id} record= {data} />     											
					}
				}

			})

	useEffect (() => {
        const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }` }
        }

        
      fetch ('https://arcane-oasis-18921.herokuapp.com/api/users/details', options)
        .then (res => res.json())
        .then(data=>{
   				console.log(data)
   				setBalance(data.balance)
        })
    
	} ,[])
		

	return(
		
		<React.Fragment>
			<Head>
				<title>Records</title>
			</Head>
			<h1>Records</h1>

			<Form.Group controlId="search" className = "w-70">
	            <Form.Control 
	                type="text"
	                placeholder="Search Record"
	                onChange={(e) => setTerm(e.target.value)}
	            />
	        </Form.Group>
	        <div>
				<select
					onChange={(e) => setFilter(e.target.value)}
				>
					<option value="">All</option>
					<option value="Income">Income</option>
					<option value="Expense">Expense</option>
				</select>
			</div>
	        <Link href="/addRecord">
		  		<Button variant="secondary">Add Record</Button>
		  	</Link>
		  	<hr/>

	       <h3 className="text-center">Remaining Amount ₱{balance}</h3>
		  	
           	<Table stripe bordered hover className="text-center">
           		<thead>
					<tr>
						<th>balance</th>
						<th>Date</th>
						<th>Type</th>
						<th>Details</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					{recordsData}
				</tbody>
			</Table>
		</React.Fragment>

	)
}