import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Dropdown, Table, Card } from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';
import Swal from 'sweetalert2';
// import {Input} from 'mdbreact';
import UserContext from '../../UserContext';

export default function index() {
    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');

    const [currentBalance, setCurrentBalance] = useState('');
    const [totalIncome, setTotalIncome] = useState('');
    const [totalExpense, setTotalExpense] = useState('');

    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [transactionDate, setTransactionDate] = useState('')
    const [description, setDescription] =useState('')
    const [amount, setAmount] = useState('');


   function addRecord(e) {

     e.preventDefault();  
  
     const recoptions = {
        method: 'POST',
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'},

        body: JSON.stringify({
          transactionDate: transactionDate,
          categoryName: categoryName,
          categoryType: categoryType,
          description: description,      
          amount: amount
        })

     }

     console.log(recoptions);


     fetch('https://arcane-oasis-18921.herokuapp.com/api/records', recoptions)
        .then (res => res.json())
        .then(data => {

            console.log(data)

            if(data) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Your transaction has been added successfully!',
                showConfirmButton: true,
              })
              .then(() => {
                setTransactionDate('')
                setCategoryName('')
                setCategoryType('')
                setDescription('')         
                setAmount('')               
                Router.push('/records')
              }) 
            }
          
          

        })

}    
       
 useEffect(() => {
  

     fetch ('https://arcane-oasis-18921.herokuapp.com/api/categories', {
      headers:
      {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }) 
    .then(res => res.json())
    .then(data => {

 
         setCategories(data.map(category => {
          if(category.categoryType === categoryType) {
            return <option value={category.categoryName} key={category._id}>{category.categoryName}</option>
          }else {
            return null
          }
        }))      
    })   

}, [categoryType]) 

  return (

      <React.Fragment>

        <h3 className="text-center">New Transaction</h3>
   
      <br></br>
        
        <Form onSubmit={(e) => addRecord(e)}>
            <Form.Group>                 
            <Form.Label htmlFor="categoryType">Category Type: </Form.Label> <Button href="/category" className="btn" variant="outline-info" size="sm">Add Category</Button>
                 <Form.Control
                    as="select"
                    placeholder="Select Category"
                    value={categoryType}
                    onChange={(e) => setCategoryType(e.target.value)}
                    required>
                    <option value="true">Select Category</option>
                    <option>Income</option>
                    <option>Expense</option>
                  </Form.Control>  
            </Form.Group> 

            <Form.Group>
              <Form.Label htmlFor="categoryName">Category Name: </Form.Label>
                 <Form.Control
                    as="select"
                    placeholder="Select Category"
                    value={categoryName}
                    onChange={(e) => setCategoryName(e.target.value)}
                    required>
                <option value="true">Select Category</option>
                {categories}
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="text">Description</Form.Label>
              <Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder="Enter Your description" required/>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="transactionDate">Transaction Date:</Form.Label>
              <Form.Control type="date" value={transactionDate} onChange={(e) => setTransactionDate(e.target.value)} placeholder="Enter Transaction Date" required/>
            </Form.Group>

            <Form.Group>
              <Form.Label htmlFor="amount">Transaction Amount:</Form.Label>
              <Form.Control type="number" value={amount} onChange={(e) => setAmount(e.target.value)} placeholder="Enter amount ₱" required/>
            </Form.Group>

            <Button type="submit" className="btn" variant="outline-primary">Submit</Button> {' '} <Button type="submit" className="btn" variant="outline-primary" href="/records">Transaction History</Button>
            
      </Form>

   </React.Fragment>   
  )

}