import React, { useContext, useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { Form } from 'react-bootstrap';

//import View from '../components/View';

export default function index() {

	const [balances, setBalances] = useState ([]);
	const [labels, setLabels] = useState ([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	console.log(startDate)
	console.log(endDate)

	
	useEffect (() => {

	const options = {
        headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
    }

	    fetch('https://arcane-oasis-18921.herokuapp.com/api/records/', options)
	    .then (res => res.json())
	    .then(data => {

				let balanceNumbers = []
				let labelNames = []
				console.log(data)
				data.forEach(record => {

					//console.log(record.transactedOn)
					// console.log(moment(record.transactedOn).isBetween(startDate, endDate, null, '[]'))
					if (startDate !== "" && endDate !== "") {
						if (moment(record.transactionDate).isBetween(startDate, endDate, null, '[]') == true) {
							balanceNumbers.push(record.balance)
							labelNames.push(record.description)

							
						}

					} else {
							balanceNumbers.push(record.balance)
							labelNames.push(record.description)
					}


					
				})

				setLabels(labelNames)
				setBalances(balanceNumbers)
		})
		console.log(moment('2021-04-18T16:00:00.000Z').isBetween(startDate, endDate, null, '[]'))

	}, [startDate, endDate])

	const data = {
		labels: labels,
		datasets: [
			{
				label: 'Budget Trend',
				backgroundColor: 'gray',
				borderColor: 'red',
				borderWidth: 1,
				hoverBackgroundColor: 'blue',
				hoverBorderColor: 'darkblue',
				data: balances
			}
		]
	}
	return (

		<React.Fragment>
		<Form.Group controlId="startDate">
                <Form.Control 
                    type="date"
                    onChange={(e) => setStartDate(e.target.value)}
                    required
                />
        </Form.Group>
        <Form.Group controlId="endDate">
                <Form.Control 
                    type="date" 
                    onChange={(e) => setEndDate(e.target.value)}
                    required
                />
        </Form.Group>
			<Line data={data} />

		</React.Fragment>
		)
}