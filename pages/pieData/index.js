import React, { useContext, useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';
import getRandomColor from 'randomcolor';
import { Form } from 'react-bootstrap';
//import View from '../components/View';

export default function index() {

	const [records, setRecords] = useState ([]);
	const [categories, setCategories] = useState ([]);
	const [categoryTotal, setCategoryTotal] = useState ([]);
	const [labels, setLabels] = useState ([]);
	const [bgColors, setBgColors] = useState([]);
	const [startDate, setStartDate] = useState('');
	console.log(startDate)

	const [endDate, setEndDate] = useState('');
	console.log(endDate)

	
	useEffect (() => {

	const options = {
        headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
    }

	    fetch('https://arcane-oasis-18921.herokuapp.com/api/records/', options)
	    .then (res => res.json())
	    .then(data=>{
	    	console.log(data)
			setRecords(data)
		})

	} ,[])

	useEffect(() => {
		const options = {
            method: 'GET',
           headers: {Authorization: `Bearer ${localStorage.getItem('token')}`, "Content-Type": 'application/json'},
        }

        fetch ('https://arcane-oasis-18921.herokuapp.com/api/categories', options)
          .then(res => res.json())
           .then(data => {
           		setCategories(data)
           })
	}, [])

	useEffect (() => {
		let total = []
		categories.forEach(category => {
			console.log(category)
			let totalPrice = 0
			records.map(record => {

				if (startDate !== "" && endDate !== "") {
					console.log('yow')
					if (moment(record.transactionDate).isBetween(startDate, endDate, null, '[]') == true) {
						if (category.categoryName == record.categoryName) {
							console.log(`hello`)
							console.log('')
							totalPrice = totalPrice + record.amount
						}
					}
				} else {
					if (category.categoryName == record.categoryName) {
					console.log(`wazzup`)
					console.log('')
					totalPrice = totalPrice + record.amount
					}
				}
			})
			console.log(totalPrice)
			total.push(totalPrice)
		})
		console.log(total)
		setCategoryTotal(total)

	},[categories,startDate,endDate])

	useEffect(() => {

		console.log(categories)
		let names = []
		categories.forEach(category => {
			// console.log(category.name)
			names.push(category.categoryName)
			// console.log(names)
		})

		setLabels(names)


	}, [categories])

	useEffect(() => {

		setBgColors(labels.map(() => getRandomColor()))

	}, [labels])


	const pieChartSettings = {
		datasets: [{
			data: categoryTotal,
			backgroundColor: bgColors
		}],
		labels:  labels
	}

	return (
		<React.Fragment>

		<Form.Group controlId="startDate" id="dateBar">
                    <Form.Control 
                        type="date"
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
            </Form.Group>
            <Form.Group controlId="endDate" id="dateBar">
                    <Form.Control 
                        type="date" 
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
            </Form.Group>
			<Pie data ={pieChartSettings} redraw={false}/>
		</React.Fragment>
		)
}