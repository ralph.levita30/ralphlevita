import React, { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext' ;

export default function Logout() {

	const { unsetUser } = useContext(UserContext);

	useEffect(() => {

		//clears the localStorage of user information
		unsetUser();

		Router.push('/')

	})

	return null
}
