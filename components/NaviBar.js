import React, { useContext } from 'react';
import Link from 'next/link';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';	
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="dark" expand="lg">
		 <Link href="#">
		 	<a className="navbar-brand text-light">Cheap</a>
		 </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
				 {(user.token !== null) ?
				<React.Fragment>
				<Link href="/pieData">
		 			<a className="nav-link text-light justify-content-left" role="button">Category Breakdown</a>
				</Link>
				<Link href="/lineChart">
		 			<a className="nav-link text-light" role="button">Budget Trend</a>
				</Link>
				<Link href="/records">
		 			<a className="nav-link text-light" role="button">Records</a>
				</Link>
				<Link href="/category">
		 			<a className="nav-link text-light" role="button">Category</a>
				</Link>
				<Link href="/logout">
		 			<a className="nav-link text-light" role="button">Logout</a>
				</Link>
				</React.Fragment>
				:
				<React.Fragment>
				</React.Fragment>
				}
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}