import React, { useContext, useState, useEffect } from 'react';
import Table from 'react-bootstrap';
import moment from 'moment';


export default function Record({record}) {
	console.log(record)

	return (
		<React.Fragment>
			<tr>
				<td>{record.balance}</td>
				<td>{moment(record.transactionDate).format('MM/DD/YYYY')}</td>
				<td>{record.categoryType}</td>
				<td>{record.description}</td>
				<td>{record.amount}</td>
			</tr>
		</React.Fragment>
	)
}